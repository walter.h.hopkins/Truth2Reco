#!/usr/bin/bash

# Install local version of pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --user
export PATH=$HOME/.local/bin:$PATH

# Install required packages. 
pip3 install keras tensorflow matplotlib scikit-learn torch --user
