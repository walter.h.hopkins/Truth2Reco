import torch, argparse, matplotlib, torch.utils.data
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from utils import *
from sklearn.model_selection import train_test_split
from glob import glob
import scipy.stats
import torchModels
modelDict = {}
for comp in dir(torchModels):
    if 'model_' in comp:
        modelDict[comp] = eval('torchModels.'+comp)

def train(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType, useCUDA, binning, parallel):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    """
    if sourceType == 'toyGauss':
        (xTrain, xTest, yTrain, yTest) = data
    else:
        xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, inputScaler, outputScaler = data
        xTrain = xTrain[:,0]
        yTrain = yTrain[:,0]
        xTest  = xTest[:,0]
        yTest  = yTest[:,0]
    # Only consider pT
    device = torch.device("cuda" if useCUDA else "cpu")
    print(device)
    xTrain = torch.autograd.Variable(torch.from_numpy(xTrain).to(dtype=torch.float32),requires_grad=False)
    xTest  = torch.autograd.Variable(torch.from_numpy(xTest).to(dtype=torch.float32),requires_grad=False)
    yTrain = torch.autograd.Variable(torch.from_numpy(yTrain).to(dtype=torch.float32),requires_grad=False)
    yTest  = torch.autograd.Variable(torch.from_numpy(yTest).to(dtype=torch.float32),requires_grad=False)
    
    fig, ax = plt.subplots()
    ax.hist(xTrain, label='source', bins=binning, histtype='step')
    ax.hist(yTrain, label='target', bins=binning, histtype='step')
    ax.legend(loc="best", borderaxespad=0.)

    plt.savefig('input.pdf', bbox_inches='tight')
    ax.set_yscale('log')
    plt.savefig('input_log.pdf', bbox_inches='tight')
    plt.close('all')

    criterion = torch.nn.BCELoss()
    #criterion = torch.nn.MSELoss()
    #criterion = torch.nn.KLDivLoss()
    gen =  modelDict[modelName+'_gen'](1, 1,
                                       outputAct=hyperParams['outputAct_gen'],
                                       hiddenSpaceSizes=hyperParams['hiddenSpaceSize_gen'],
                                       activations=hyperParams['activations_gen'], parallel=parallel
    ).to(device)

    discr =  modelDict[modelName+'_discr'](1, 1,
                                           outputAct=hyperParams['outputAct_discr'],
                                           hiddenSpaceSizes=hyperParams['hiddenSpaceSize_discr'],
                                           activations=hyperParams['activations_discr'], parallel=parallel
    ).to(device)

    # Establish convention for real and fake labels during training
    real_label = 1
    fake_label = 0
    
    # Setup Adam optimizers for both G and D
    optimizerD = torch.optim.Adam(discr.parameters(), lr=hyperParams['discrLR'])
    optimizerG = torch.optim.Adam(gen.parameters(), lr=hyperParams['genLR'])

    kwargs = {'num_workers': 4, 'pin_memory': True} if useCUDA else {}
    trainingSet = torch.utils.data.TensorDataset(xTrain, yTrain)
    #testingSet  = torch.utils.data.TensorDataset(xTest, yTest)
    trainLoader = torch.utils.data.DataLoader(trainingSet, batch_size=batchSize, shuffle=True, **kwargs)
    #testLoader  = torch.utils.data.DataLoader(testingSet, batch_size=batchSize, shuffle=True)

    G_losses = []
    D_losses = []
    for epoch in range(epochs):  # loop over the dataset multiple running
        print('\nGoing through training set for epoch', epoch, '/', epochs)
        batchI = 0;
        label = torch.full((batchSize,), real_label).to(device)

        for xTrainBatch, yTrainBatch in trainLoader:
            xTrainBatch, yTrainBatch = xTrainBatch.to(device), yTrainBatch.to(device)
            discr.zero_grad()
            sourceOutput = discr(yTrainBatch.view(-1, 1))
            label.fill_(real_label)

            lossD_real = criterion(sourceOutput, label)
            lossD_real.backward()

            genPredBatch = gen(xTrainBatch.view(-1, 1))
            label.fill_(fake_label)
            targetOutput = discr(genPredBatch)
            lossD_gen = criterion(targetOutput, label)
            lossD_gen.backward()

            lossD = lossD_real+lossD_gen
            optimizerD.step()

            gen.zero_grad()
            label.fill_(real_label)
            genPredBatch = gen(xTrainBatch.view(-1, 1))
            targetOutput = discr(genPredBatch)
            lossG = criterion(targetOutput, label)
            lossG.backward()
            optimizerG.step()
            batchI+=1
            #break;
            
        print('Went through', batchI, 'batches for training\n')
        print('Going through validation set')
        # Now calculate the validation binned loss
        testPred = gen(xTest.view(-1,1).to(device)).detach().view(-1).cpu().numpy()
        
        torch.save(
            {'model_state_dict':gen.state_dict(),
            },
            modelFPath+'/'+modelName+'_gen_'+str(epoch)
        )
        print("Done going through the validation set.")
        G_losses.append(lossG.item())
        D_losses.append(lossD.item())
        print(epoch, round(lossG.item(),2), round(lossD.item(),3))
        print("Done calculating binned error, plotting if epoch is multiple of 100.")
        if epoch % 100 == 0:
            plt.clf()
            fig, ax = plt.subplots()
            plt.plot(G_losses, label='gen')
            plt.plot(D_losses, label='critic')
            ax.set_xlabel('epoch')
            ax.set_ylabel('Loss')
            leg = plt.legend(loc=0, borderaxespad=0.)
            plt.savefig('loss_'+modelName+'_'+sourceType+'_torch.pdf', bbox_inches='tight')
            plt.savefig('loss_'+modelName+'_'+sourceType+'_torch.svg', bbox_inches='tight')
            print("Done plotting for epoch", epoch)

    print('Finished training model')

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(G_losses, label='gen')
    plt.plot(D_losses, label='critic')
    ax.set_xlabel('epoch')
    ax.set_ylabel('Loss')
    leg = plt.legend(loc=0, borderaxespad=0.)
    plt.savefig('loss_'+modelName+'_'+sourceType+'_torch.pdf', bbox_inches='tight')
    plt.savefig('loss_'+modelName+'_'+sourceType+'_torch.svg', bbox_inches='tight')

def test(data, modelName, modelFPath, hyperParams, sourceType, useCUDA, binning):
    
    if sourceType == 'toyGauss':
        (xTrain, xTest, yTrain, yTest) = data
    else:
        xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, inputScaler, outputScaler = data
        xTrain = xTrain[:,0]
        yTrain = yTrain[:,0]
        xTest  = xTest[:,0]
        yTest  = yTest[:,0]
    # Only consider pT
    device = torch.device("cuda" if useCUDA else "cpu")
    print(device)
    xTrain = torch.autograd.Variable(torch.from_numpy(xTrain).to(dtype=torch.float32),requires_grad=False)
    xTest  = torch.autograd.Variable(torch.from_numpy(xTest).to(dtype=torch.float32),requires_grad=False)
    yTrain = torch.autograd.Variable(torch.from_numpy(yTrain).to(dtype=torch.float32),requires_grad=False)
    yTest  = torch.autograd.Variable(torch.from_numpy(yTest).to(dtype=torch.float32),requires_grad=False)
    
    fig, ax = plt.subplots()
    ax.hist(xTrain, label='source', bins=binning)
    ax.hist(yTrain, label='target', bins=binning)
    ax.legend(loc="best", borderaxespad=0.)

    plt.savefig('input.pdf', bbox_inches='tight')
    plt.close('all')

    ksVals = []
    genFiles = glob(modelFPath+'/'+modelName+'_gen_*')
    gens = []
    yGenTests = []
    for genFileI in range(len(genFiles)):
        genFile = modelFPath+'/'+modelName+'_gen_'+str(genFileI)
        gen = modelDict[modelName+'_gen'](1, 1,
                                          outputAct=hyperParams['outputAct_gen'],
                                          hiddenSpaceSizes=hyperParams['hiddenSpaceSize_gen'],
                                          activations=hyperParams['activations_gen']
        ).to(device)
        checkpoint=torch.load(genFile)
        gen.load_state_dict(checkpoint['model_state_dict'])
        gen.eval()
        yGenTest = gen(xTest.view(-1,1).to(device)).detach().view(-1).cpu().numpy()
        ks, pVal = scipy.stats.ks_2samp(yTest, yGenTest)
        ksVals.append(ks)
        gens.append(gen)
        yGenTests.append(yGenTest)

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(ksVals, label='KS-test (pred, true)')
    ax.set_xlabel('epoch')
    ax.set_ylabel('KS value')
    leg = plt.legend(loc=0, borderaxespad=0.)
    ax.set_ylim(0, 1.3*max(ksVals))

    plt.savefig('ks_'+modelName+'_'+sourceType+'_torch.pdf', bbox_inches='tight')
    plt.savefig('ks_'+modelName+'_'+sourceType+'_torch.svg', bbox_inches='tight')
    
    bestEpoch = np.argmin(np.array(ksVals))
    print("The best epoch was", bestEpoch)
    bestYGenTest = yGenTests[bestEpoch]
    
    plt.clf()
    fig, ax = plt.subplots()
    ax.hist(xTest, label='source', histtype='step', bins=binning)
    ax.hist(yTest, label='target', histtype='step', bins=binning)
    ax.hist(bestYGenTest, label='targetBestGen', histtype='step', bins=binning)
    ax.legend(loc="best", borderaxespad=0.)
    ax.set_ylim(0, 1.4*ax.get_ylim()[1])

    plt.savefig('best_output_torch.pdf', bbox_inches='tight')
    plt.savefig('best_output_torch.svg', bbox_inches='tight')
    ax.set_ylim(0.1, 5*ax.get_ylim()[1])

    ax.set_yscale('log')
    plt.savefig('best_output_torch_log.pdf', bbox_inches='tight')
    plt.savefig('best_output_torch_log.svg', bbox_inches='tight')
    plt.close('all')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='rfast008.h5')
    parser.add_argument('--modelName', help='Name of models. Comma delimited if used for tested.', default='model_gan')
    parser.add_argument('--trainingFunc', help='Name of training function to use.', default='trainGAN')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toyGauss', 'other'])
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=80000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=20000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=2000)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--uniformityProp', type=int, help='Seed for randomizor', default=None)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='GAN')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')
    parser.add_argument('--useCUDA', action='store_true', help='Use cuda')
    parser.add_argument('--parallel', action='store_true', help='Parallel training')

    args = parser.parse_args()
    sourceData = np.random.normal(0.0, 1.0, 100000)
    targetData = np.random.normal(8.0, 1.0, 100000)
    # sourceData = np.random.normal(10.0, 1.0, 100000)
    # targetData = np.random.normal(20.0, 1.0, 100000)

    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print("Producing new hyperparameter file.")
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
        except IOError as e:
            print("Couldn't load "+args.hyperParamFPath)
            print("Producing new hyperparameter file.")
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    
    if args.hyperParamKey not in allHyperParams:
        print("Couldn't find hyperparameter set with key", args.hyperParamKey)
        sys.exit()

    if args.sourceType == 'toyGauss':
        data = train_test_split(sourceData, targetData, test_size=args.nTestSamp, train_size=args.nTrainSamp, random_state=args.randomSeed)
        binning=[i-4 for i in range(25)]
    else:
        data = prepMatchedData(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed, sourceType=args.sourceType, uniformityProp=args.uniformityProp);
        binning=[-0.05+i*0.05 for i in range(21)]

    np.random.seed(args.randomSeed)
    torch.manual_seed(args.randomSeed)

    if 'train' in args.trainTest:
        train(data, args.modelName, args.modelFPath, allHyperParams[args.hyperParamKey],
              args.sourceDSName, args.targetDSName, args.epochs, args.batchSize, args.hyperParamKey,
              args.sourceType, args.useCUDA, binning, args.parallel);
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, allHyperParams[args.hyperParamKey], args.sourceType, args.useCUDA, binning);

