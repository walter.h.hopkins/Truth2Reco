#!/bin/bash

# setup tensorflow v2 environment
source /direct/u0b/software/jupyter/virtenvs/tf2venv/bin/activate
cd /usatlas/u/whopkins/Truth2Reco
echo $PWD

# You have to have the hyperparameter and scaler files already set up.
python3 trainRes.py train --modelName model_multiClass --hyperParamKey resMultiC  --epochs $1 --batchSize $2 --h5Path rfast008_new_big.h5 --nResCats 200
