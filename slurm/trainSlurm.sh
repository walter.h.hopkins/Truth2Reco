#!/bin/bash
#SBATCH --partition usatlas
#SBATCH --time=24:00:00
#SBATCH --account=tier3
#SBATCH -N 3
#SBATCH --gpus-per-node=1
#SBATCH --qos usatlas

#for f in 2 5 10 20 100 200 1000 2000 10000 20000
for f in 1000
do
		srun -G1 -N1 ./train.sh $f $(( 1*f )) >& logs/$f.log & 
done

wait
