#!/usr/bin/env python

from ROOT import TH1F, TFile, TCanvas, gROOT, TPad, gPad, TLine, TChain
import argparse, os, math
from setStyle import setStyle, CompLegend

compDict = {
    'AntiKt4Jets':['AntiKt4TruthJets', 'AntiKt4NNJets'],
    }

# For how many jets are we making plots
nJets = 2
jetVarNames = ['pt', 'eta', 'phi']
varNames = [
    ]
varNamesUnits = {
    'eta':('#eta', ''),
    'pt':('p_{T}', 'GeV'),
    'phi':('#phi', ''),
    }
binning = {
    'eta':[50, -5, 5],
    'pt':[30, 0, 600],
    'phi':[32, -3.2, 3.2], 
    }

for jetVarName in jetVarNames:
    for jetI in range(nJets):
        varNames.append(jetVarName+"["+str(jetI)+"]")
        varNamesUnits[jetVarName+"["+str(jetI)+"]"] = (varNamesUnits[jetVarName][0]+"^{"+str(jetI)+"}", varNamesUnits[jetVarName][1])
        binning[jetVarName+"["+str(jetI)+"]"] = binning[jetVarName]
        
def distComp(inFPath):
    """! 
    Plot two distributions on the same plot and also plot the ratio of the two. 
    The input file must have trees.
    """
    gROOT.SetBatch(True);
    gROOT.ForceStyle();
    setStyle()
    
    trees = {}
    for treeName in compDict.keys()+[tempName for tempList in compDict.values() for tempName in tempList]:
        trees[treeName] = TChain(treeName) 
        trees[treeName].Add(inFPath+"/*");

    c = TCanvas('c', '', 800, 600)
    c.cd();
    pad = TPad("plotPad","",0,0,1,1);
    pad.SetPad(0.,0.37,1.,1.)
    pad.SetBottomMargin(0.02)
    pad.Draw();
    c.cd()
    ratioPad = TPad("ratioPad", "",0.,0.,1.,1.);
    ratioPad.SetPad(0.,0.05,1.,0.35)
    ratioPad.SetTopMargin(0.04)
    ratioPad.SetBottomMargin(0.3)
    ratioPad.Draw()
    refHists   = {}
    compHists  = {}
    ratioHists = {}
    for varName in varNames:
        pad.Clear();
        pad.SetLogy(False)
        ratioPad.Clear();
        for baseTreeName in compDict:
            baseTree = trees[baseTreeName]
            pad.cd()
            refHistName = 'hist_'+baseTreeName+'_'+varName.replace('[','_').replace(']','_')
            refHists[varName] = TH1F(refHistName, '', binning[varName][0], binning[varName][1], binning[varName][2])
            baseTree.Draw(varName+'>>'+refHistName)
            compHists[varName] = []
            ratioHists[varName] = []
            refHists[varName].Scale(1./refHists[varName].Integral())
            maxY = refHists[varName].GetMaximum()
            for inTreeI in range(len(compDict[baseTreeName])):
                compHistName = 'hist_'+compDict[baseTreeName][inTreeI]+'_'+varName.replace('[','_').replace(']','_')
                compHist = TH1F(compHistName, '', binning[varName][0], binning[varName][1], binning[varName][2])
                trees[compDict[baseTreeName][inTreeI]].Draw(varName+'>>'+compHistName);
                compHists[varName].append(compHist)
                compHists[varName][inTreeI].Scale(1./compHists[varName][inTreeI].Integral())

                if maxY < compHists[varName][inTreeI].GetMaximum():
                    maxY = compHists[varName][inTreeI].GetMaximum()
                
            pad.Clear();
            refHists[varName].SetTitle('')
            refHists[varName].SetMaximum(1.2*maxY)
            refHists[varName].Draw();
            unitsStr = ''
            if varNamesUnits[varName][1] != '':
                unitsStr = ' ['+varNamesUnits[varName][1]+']'
            refHists[varName].GetXaxis().SetTitle(varNamesUnits[varName][0]+unitsStr)
            binSize = refHists[varName].GetBinLowEdge(2)-refHists[varName].GetBinLowEdge(1)
            roundTo = -int(math.floor(math.log10(abs(binSize))));
            roundedBinSize = round(binSize, roundTo)
            if roundTo < 0:
                binSizeStr = "%d" % roundedBinSize
            else:
                binSizeStr = "{0:.{1}f}".format(roundedBinSize, roundTo)
            yLabel = "1/N dN/(%s) [%s %s]^{-1}" % (varNamesUnits[varName][0], binSizeStr, varNamesUnits[varName][1])

            refHists[varName].GetYaxis().SetTitle(yLabel)
            
            ratioPad.Clear()
            for inTreeI in range(len(compDict[baseTreeName])):
                pad.cd()
                compHists[varName][inTreeI].SetLineColor(inTreeI+2);
                compHists[varName][inTreeI].SetMarkerColor(inTreeI+2);
                compHists[varName][inTreeI].Draw('same')
                ratioPad.cd()

                ratioHists[varName].append(refHists[varName].Clone())
                ratioHists[varName][inTreeI].GetYaxis().SetTitleOffset(0.4)
                ratioHists[varName][inTreeI].GetYaxis().SetDecimals(True);
                ratioHists[varName][inTreeI].GetYaxis().CenterTitle(True);
                ratioHists[varName][inTreeI].Divide(compHists[varName][inTreeI])
                ratioHists[varName][inTreeI].SetMaximum(2)
                ratioHists[varName][inTreeI].SetMinimum(0.)
                ratioHists[varName][inTreeI].GetYaxis().SetTitleSize(compHists[varName][inTreeI].GetXaxis().GetTitleSize()*2.1)
                ratioHists[varName][inTreeI].GetXaxis().SetTitleSize(compHists[varName][inTreeI].GetYaxis().GetTitleSize()*2.1)

                ratioHists[varName][inTreeI].GetYaxis().SetLabelSize(compHists[varName][inTreeI].GetXaxis().GetLabelSize()*2.1)
                ratioHists[varName][inTreeI].GetXaxis().SetLabelSize(compHists[varName][inTreeI].GetYaxis().GetLabelSize()*2.1)
                ratioHists[varName][inTreeI].GetYaxis().SetTitle('Reco/Alt');
                
                ratioHists[varName][inTreeI].SetLineColor(inTreeI+2);
                ratioHists[varName][inTreeI].SetMarkerColor(inTreeI+2);
                if inTreeI == 0:
                    ratioHists[varName][inTreeI].Draw()
                else:
                    ratioHists[varName][inTreeI].Draw('same')
            line = TLine()
            line.DrawLine(refHists[varName].GetXaxis().GetXmin(),1,refHists[varName].GetXaxis().GetXmax(),1)
            line.SetLineWidth(1);
            line.SetLineStyle(2);
            line.SetLineColor(1);
            c.cd();
            leg = CompLegend((0.7, 0.8, 0.9, 0.9), [refHists[varName]]+compHists[varName], [baseTree.GetName()]+compDict[baseTreeName])
            leg.Draw('same')
            c.Print(varName.replace('/','_').replace('[','_').replace(']','_').rstrip('_').lstrip('_')+'.pdf')
            pad.SetLogy();
            refHists[varName].SetMaximum(10*maxY)
            c.Print(varName.replace('/','_').replace('[','_').replace(']','_').rstrip('_').lstrip('_')+'_log.pdf')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compare jet kinematics between different trees ')
    parser.add_argument('--inFPath', help='Location of ROOT files.', default='out_root/')

    args = parser.parse_args() 
    distComp(args.inFPath)


