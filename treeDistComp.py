#!/usr/bin/env python

from ROOT import TH1F, TFile, TCanvas, gROOT, TPad, gPad, TLine, TChain
import argparse, os, math
from setStyle import setStyle, CompLegend

compDict = {
    'Jet.PT[0]':['GenJet.PT[0]'],
    'Jet.Eta[0]':['GenJet.Eta[0]'],
    'Jet.Phi[0]':['GenJet.Phi[0]'],

    'Jet.PT[1]':['GenJet.PT[1]'],
    'Jet.Eta[1]':['GenJet.Eta[1]'],
    'Jet.Phi[1]':['GenJet.Phi[1]'],
    }

varNamesUnits = {
    'Jet.Eta[0]':('#eta', ''),
    'Jet.PT[0]':('p_{T}', 'GeV'),
    'Jet.Phi[0]':('#phi', ''),
    
    'Jet.Eta[1]':('#eta', ''),
    'Jet.PT[1]':('p_{T}', 'GeV'),
    'Jet.Phi[1]':('#phi', ''),
    }
binning = {
    'Jet.Eta[0]':[50, -5, 5],
    'Jet.PT[0]':[30, 0, 600],
    'Jet.Phi[0]':[32, -1.*math.pi, math.pi],
    
    'Jet.Eta[1]':[50, -5, 5],
    'Jet.PT[1]':[30, 0, 600],
    'Jet.Phi[1]':[32, -1.*math.pi, math.pi], 
    }

def distComp(inFPath, treeName):
    """! 
    Plot two distributions on the same plot and also plot the ratio of the two. 
    The input file must have trees.
    """
    gROOT.SetBatch(True);
    gROOT.ForceStyle();
    setStyle()
    tree = TChain(treeName) 
    tree.Add(inFPath+"*");

    c = TCanvas('c', '', 800, 600)
    c.cd();
    pad = TPad("plotPad","",0,0,1,1);
    pad.SetPad(0.,0.37,1.,1.)
    pad.SetBottomMargin(0.02)
    pad.Draw();
    c.cd()
    ratioPad = TPad("ratioPad", "",0.,0.,1.,1.);
    ratioPad.SetPad(0.,0.05,1.,0.35)
    ratioPad.SetTopMargin(0.04)
    ratioPad.SetBottomMargin(0.3)
    ratioPad.Draw()
    refHists   = {}
    compHists  = {}
    ratioHists = {}
    for varName in compDict:
        pad.Clear();
        pad.SetLogy(False)
        ratioPad.Clear();
        pad.cd()
        refHistName = 'hist_'+varName.replace('[','_').replace(']','_')
        refHists[varName] = TH1F(refHistName, '', binning[varName][0], binning[varName][1], binning[varName][2])
        tree.Draw(varName+'>>'+refHistName)
        compHists[varName] = []
        ratioHists[varName] = []
        refHists[varName].Scale(1./refHists[varName].Integral())
        maxY = refHists[varName].GetMaximum()
        for inTreeI in range(len(compDict[varName])):
            compHistName = 'hist_'+compDict[varName][inTreeI]+'_'+varName.replace('[','_').replace(']','_')
            compHist = TH1F(compHistName, '', binning[varName][0], binning[varName][1], binning[varName][2])
            tree.Draw(compDict[varName][inTreeI]+'>>'+compHistName);
            compHists[varName].append(compHist)
            compHists[varName][inTreeI].Scale(1./compHists[varName][inTreeI].Integral())

            if maxY < compHists[varName][inTreeI].GetMaximum():
                maxY = compHists[varName][inTreeI].GetMaximum()

        pad.Clear();
        refHists[varName].SetTitle('')
        refHists[varName].SetMaximum(1.4*maxY)
        refHists[varName].SetMinimum(0.0001)
        refHists[varName].Draw();
        unitsStr = ''
        if varNamesUnits[varName][1] != '':
            unitsStr = ' ['+varNamesUnits[varName][1]+']'
        refHists[varName].GetXaxis().SetTitle(varNamesUnits[varName][0]+unitsStr)
        binSize = refHists[varName].GetBinLowEdge(2)-refHists[varName].GetBinLowEdge(1)
        roundTo = -int(math.floor(math.log10(abs(binSize))));
        roundedBinSize = round(binSize, roundTo)
        if roundTo < 0:
            binSizeStr = "%d" % roundedBinSize
        else:
            binSizeStr = "{0:.{1}f}".format(roundedBinSize, roundTo)
        yLabel = "1/N dN/(%s) [%s %s]^{-1}" % (varNamesUnits[varName][0], binSizeStr, varNamesUnits[varName][1])

        refHists[varName].GetYaxis().SetTitle(yLabel)

        ratioPad.Clear()
        for inTreeI in range(len(compDict[varName])):
            pad.cd()
            compHists[varName][inTreeI].SetLineColor(inTreeI+2);
            compHists[varName][inTreeI].SetMarkerColor(inTreeI+2);
            compHists[varName][inTreeI].Draw('same')
            ratioPad.cd()

            ratioHists[varName].append(refHists[varName].Clone())
            ratioHists[varName][inTreeI].GetYaxis().SetTitleOffset(0.4)
            ratioHists[varName][inTreeI].GetYaxis().SetDecimals(True);
            ratioHists[varName][inTreeI].GetYaxis().CenterTitle(True);
            ratioHists[varName][inTreeI].Divide(compHists[varName][inTreeI])
            ratioHists[varName][inTreeI].SetMaximum(2)
            ratioHists[varName][inTreeI].SetMinimum(0.)
            ratioHists[varName][inTreeI].GetYaxis().SetTitleSize(compHists[varName][inTreeI].GetXaxis().GetTitleSize()*2.1)
            ratioHists[varName][inTreeI].GetXaxis().SetTitleSize(compHists[varName][inTreeI].GetYaxis().GetTitleSize()*2.1)

            ratioHists[varName][inTreeI].GetYaxis().SetLabelSize(compHists[varName][inTreeI].GetXaxis().GetLabelSize()*2.1)
            ratioHists[varName][inTreeI].GetXaxis().SetLabelSize(compHists[varName][inTreeI].GetYaxis().GetLabelSize()*2.1)
            ratioHists[varName][inTreeI].GetYaxis().SetTitle('Reco/Alt');

            ratioHists[varName][inTreeI].SetLineColor(inTreeI+2);
            ratioHists[varName][inTreeI].SetMarkerColor(inTreeI+2);
            if inTreeI == 0:
                ratioHists[varName][inTreeI].Draw()
            else:
                ratioHists[varName][inTreeI].Draw('same')
            line = TLine()
            line.DrawLine(refHists[varName].GetXaxis().GetXmin(),1,refHists[varName].GetXaxis().GetXmax(),1)
            line.SetLineWidth(1);
            line.SetLineStyle(2);
            line.SetLineColor(1);
            c.cd();
            leg = CompLegend((0.7, 0.8, 0.9, 0.9), [refHists[varName]]+compHists[varName], [varName]+compDict[varName])
            leg.Draw('same')
            c.Print(varName.replace('/','_').replace('[','_').replace(']','_').rstrip('_').lstrip('_')+'.pdf')
            pad.SetLogy();
            refHists[varName].SetMaximum(10*maxY)
            c.Print(varName.replace('/','_').replace('[','_').replace(']','_').rstrip('_').lstrip('_')+'_log.pdf')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compare jet kinematics between different collections with one tree')
    parser.add_argument('--inFPath', help='Location of ROOT files.', default='/netapp/data/hepsim/events/pp/14tev/mg5_ttbar_jet/rfast004/mg5_ttbar_jet_001.root')
    parser.add_argument('--treeName', help='Name of tree that has all information to be compared', default='Delphes')

    args = parser.parse_args() 
    distComp(args.inFPath, args.treeName)


