#!/usr/bin/env python

import numpy as np
import math, random
from utils import *
import timeit

def getSmear(v, pt, scale=1, A=0.5, B=0.03, RESPONSE=0.98, wrapVals=False):
     """!
     Apply some detector smearing and shift true value v
     
     input:  v - value to be smeared
            pt - the value to be smeared  usually depends on energy (pT)
            scale - scale factor. For photons, electrons, muons, set to 0.1

     Suggested variations for optional parameters:
     RESPONSE: 0.95-1.0 
     A:        0.6-0.02 stochastic term of a detector smearing for jet 
     B:        0.01-0.03 constant term of detector smearing 
     """
     sigma=scale*(B*pt+A*math.sqrt(pt))
     smearQuant = random.gauss(v*RESPONSE, sigma)
     # This is really for phi wrapping since objects don't
     # go out of acceptance at any phi.
     if wrapVals:
          while smearQuant >= math.pi: smearQuant-=2*math.pi;
          while smearQuant < -1*math.pi: smearQuant+=2*math.pi;

     return smearQuant

def produceObjects(nObjects=1000000, propertyBounds=[(0, 2000), (-3,3), (-1*math.pi, math.pi), (100,200)], scales=[1., 0.001, 0.001, 1.], wrapVals=[False, False, True, False]):
     """! 
     Changed Sergei's output to use numpy arrays and store everything in an HDF5 file.
     The format of the numpy array should be objI, fourVecI
     """
     # Produce "truth" particles which will all have a uniform distribution based on
     # the bounds set in propertyBounds.
     truthObjects = np.zeros((nObjects, len(propertyBounds)))
     # A bit awkward but the pdf functions have to take at least two arguments. 
     pdfs = [np.random.normal, np.random.uniform, np.random.uniform, np.random.normal]
     getAbs = [True, False, False, True]
     for pI in range(len(propertyBounds)): 
          truthObjects[:,pI] = pdfs[pI](propertyBounds[pI][0], propertyBounds[pI][1], nObjects)
          if getAbs[pI]:
               truthObjects[:,pI] = np.absolute(truthObjects[:,pI])
     vectSmear = np.vectorize(getSmear)
     
     recoObjects = np.zeros((nObjects, len(propertyBounds)))
     for pI in range(len(propertyBounds)):
          recoObjects[:,pI] = vectSmear(truthObjects[:,pI], truthObjects[:,0], scale=scales[pI], wrapVals=wrapVals[pI])
    
     # This is just to show how vectorizing helps make this an order of magnitude faster.
     origScaleBinning = [[i*50 for i in range(51)], [-3.5+0.1*i for i in range(71)], [(-1.5+1./10*i)*math.pi for i in range(31)], [i*10 for i in range(41)]]

     return truthObjects, recoObjects
          
if __name__ == '__main__':
    produceObjects(nObjects=10000)
