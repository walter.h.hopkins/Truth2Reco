#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math, re
import numpy as np

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import model_from_json

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)
        
from plottingTools import *
from utils import *
import scipy.stats

def train(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochsSet, batchSizes, hyperParamKey, sourceType):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoObjectsTrain, recoObjectsTest, yTrain, yTest, inputScaler, resScalers,  minScaledVal, maxScaledVal, resMinScaledVal, resMaxScaledVal, uniformityHist) = data
    for featI in range(1):#yTrain.shape[1]):
        for batchSize, epochs in zip(batchSizes,epochsSet):
            featName = featNames[featI]
            print("Training network for feature", featName)
            jSONSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_batchSize_'+str(batchSize)+'_epochs_'+str(epochs)+'_nCats_'+str(hyperParams['outputDim'])+'.json'
            weightsSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_batchSize_'+str(batchSize)+'_epochs_'+str(epochs)+'_nCats_'+str(hyperParams['outputDim'])+'.h5'
            model = modelDict[modelName](xTrain.shape[1:], hyperParams['outputDim'], jSONSavePath,
                                         outputAct=hyperParams['outputAct'],
                                         hiddenSpaceSize=hyperParams['hiddenSpaceSize'],
                                         activations=hyperParams['activations']
            )
            history = model.fit(xTrain, yTrain[:,featI,:], epochs=epochs, batch_size=batchSize, shuffle=True, validation_data=(xTest, yTest[:,featI,:], uniformityWeightsTest), sample_weight=uniformityWeightsTrain)
            model.save_weights(weightsSavePath)

            plt.clf()
            fig, ax = plt.subplots()
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            ax.set_xlabel('epoch')
            ax.set_ylabel('Loss')
            leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
            plt.savefig('loss_'+modelName+'_'+sourceType+"_"+featName+'batchSize_'+str(batchSize)+'_nCats_'+str(hyperParams['outputDim'])+'.pdf', bbox_inches='tight')
            plt.savefig('loss_'+modelName+'_'+sourceType+"_"+featName+'batchSize_'+str(batchSize)+'_nCats_'+str(hyperParams['outputDim'])+'.svg', bbox_inches='tight')
            plt.savefig('loss_'+modelName+'_'+sourceType+"_"+featName+'batchSize_'+str(batchSize)+'_nCats_'+str(hyperParams['outputDim'])+'.eps', bbox_inches='tight')


def test(data, modelName, modelFPath, sourceDSName, targetDSName, hyperParamKey, sourceType, nResCats, epochsSet, batchSizes, printSum=True, trainingFeatures=[1,2], phiIndex=2):
    """! 
    This function tests a model given an h5 file with a training source and target data set. 
    """
    print("Loading data")
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoObjectsTrain, recoObjectsTest, yTrain, yTest, inputScaler, resScaler, minScaledVal, maxScaledVal, resMinScaledVal, resMaxScaledVal, uniformityHist) = data
    print("Done loading data")
    
    binning = [i*(1./nResCats) for i in range(nResCats+1)]
    profileBinning = [i*0.05 for i in range(25)]
    featBinning = [-0.02+i*0.02 for i in range(52)]

    recoObjectsTestOrigScale  = np.copy(xTest[:,trainingFeatures])
    recoObjectsTrainOrigScale = np.copy(xTrain[:,trainingFeatures])

    yGenTrain = {}
    yGenTest = {}
    
    corrTruthObjectsTestOrigScale  = {}
    corrTruthObjectsTrainOrigScale = {}

    for batchSize in batchSizes:
        yGenTrain[batchSize] = np.zeros(yTrain.shape)
        yGenTest[batchSize] = np.zeros(yTest.shape)
        
        corrTruthObjectsTestOrigScale[batchSize]  = np.copy(xTest[:,trainingFeatures])
        corrTruthObjectsTrainOrigScale[batchSize] = np.copy(xTrain[:,trainingFeatures])

    models = {}        
    # We have a model for each resolution we want to model. Loop over resolutions and make predictions that will be
    # store in numpy arrays.
    
    for featI in range(1):#yTrain.shape[1]):
        models[featI] = {}
        for batchSize, epochs in zip(batchSizes, epochsSet):
            featName = featNames[featI]
            print("Loading model for", featName)
            modelJSONPath    = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_batchSize_'+str(batchSize)+'_epochs_'+str(epochs)+'_nCats_'+str(nResCats)+'.json'
            modelWeightsPath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_batchSize_'+str(batchSize)+'_epochs_'+str(epochs)+'_nCats_'+str(nResCats)+'.h5'
            json_file        = open(modelJSONPath, 'r')
            model_json       = json_file.read()
            json_file.close()
            model = model_from_json(model_json)
            # Load weights into new model
            model.load_weights(modelWeightsPath)
            print("Done loading from", modelJSONPath, modelWeightsPath)
            if printSum:
                model.summary()
            models[featI][batchSize]=model
        
    for featI in range(1):#yTrain.shape[1]):
        for batchSize in batchSizes:
            print('Generating feature', featI)
            yGenTest[batchSize][:,featI] = transformObjects(xTest, models[featI][batchSize],
                                                            corrTruthObjectsTestOrigScale[batchSize], inputScaler,
                                                            resScaler, featI, nResCats, resMinScaledVal,
                                                            resMaxScaledVal, uniformityHist=uniformityHist)
            yGenTrain[batchSize][:,featI] = transformObjects(xTrain, models[featI][batchSize],
                                                             corrTruthObjectsTrainOrigScale[batchSize], inputScaler,
                                                             resScaler, featI, nResCats, resMinScaledVal,
                                                             resMaxScaledVal, uniformityHist=uniformityHist)

    # Now transform the other features only for 
    # Transform the inputs into the original scale now so we can use them to get generated NN based reco objects.    
    truthObjectsTestOrigScale  = inputScaler.inverse_transform(xTest)
    truthObjectsTrainOrigScale = inputScaler.inverse_transform(xTrain)

    recoObjectsTestOrigScale  = inputScaler.inverse_transform(recoObjectsTest)[:,trainingFeatures]
    recoObjectsTrainOrigScale = inputScaler.inverse_transform(recoObjectsTrain)[:,trainingFeatures]

    etaBoundary = 3.2
    lowEtaIdxTest = (abs(truthObjectsTestOrigScale[:,1]) < etaBoundary) 
    highEtaIdxTest = (abs(truthObjectsTestOrigScale[:,1]) > etaBoundary) 
    lowEtaIdxTrain = (abs(truthObjectsTrainOrigScale[:,1]) < etaBoundary) 
    highEtaIdxTrain = (abs(truthObjectsTrainOrigScale[:,1]) > etaBoundary)

    pTCut = 100
    lowEtaLowPtIdxTest = (abs(truthObjectsTestOrigScale[:,0]) < pTCut) & lowEtaIdxTest
    highEtaLowPtIdxTest = (abs(truthObjectsTestOrigScale[:,0]) < pTCut) & highEtaIdxTest
    lowEtaLowPtIdxTrain = (abs(truthObjectsTrainOrigScale[:,0]) < pTCut) & lowEtaIdxTrain
    highEtaLowPtIdxTrain = (abs(truthObjectsTrainOrigScale[:,0]) < pTCut) & highEtaIdxTrain

    lowEtaHighPtIdxTest = (abs(truthObjectsTestOrigScale[:,0]) > pTCut) & lowEtaIdxTest
    highEtaHighPtIdxTest = (abs(truthObjectsTestOrigScale[:,0]) > pTCut) & highEtaIdxTest
    lowEtaHighPtIdxTrain = (abs(truthObjectsTrainOrigScale[:,0]) > pTCut) & lowEtaIdxTrain
    highEtaHighPtIdxTrain = (abs(truthObjectsTrainOrigScale[:,0]) > pTCut) & highEtaIdxTrain

    print("Number of test high eta jets:", np.sum(highEtaHighPtIdxTest))
    print("Number of train high eta jets:", np.sum(highEtaHighPtIdxTrain))
    realResHist = np.sum(yTest, axis=0)
    genResHist  = {}
    for batchSize in batchSizes:
        genResHist[batchSize]  = np.sum(yGenTest[batchSize], axis=0)
    # Now let's make some plots.
    for featI in range(1):#yTrain.shape[1]):
        featName       = featNames[featI]
        prettyVarLabel = ratioLabels[featI]
        rmsLabel       = rmsLabels[featI]
        featLabel      = featLabels[featI]
        featLabelTruth = featLabelsTruth[featI]
        highKsVals = []
        lowKsVals = []
        for batchSize, epochs in zip(batchSizes,epochsSet):
            batchSizeStr="_batchSize"+str(batchSize)+'_epochs'+str(epochs)+'_nCats'+str(nResCats)
            compareDists([realResHist[featI,:], genResHist[batchSize][featI,:]], ['Delphes', 'NN'],
                         np.array(binning), ['k', 'r'], 'jet',
                         featName+'Res'+batchSizeStr, prettyVarLabel+' scaled', log=False,
                         normalize=True, plotRatio=False, markerfacecolors=['k', 'none'], yLabel="arbitrary units")

            # Let's plot the NN output for some random events.
            randObj = np.random.randint(xTest.shape[0], size=3)
            plt.clf()
            fig, ax = plt.subplots()
            for randI in range(len(randObj)):
                ax.plot([i for i in range(nResCats)], yGenTest[batchSize][randObj[randI],featI,:],
                        label='Rand jet '+str(randI), linewidth=5)
            ax.set_xlabel('NN category for '+re.sub('\[.*?\]', '', prettyVarLabel))
            ax.set_ylabel('Category probability')
            ax.legend(loc=0, borderaxespad=0.)
            plt.savefig('nnOutput_'+featName+batchSizeStr+'.pdf', bbox_inches='tight')
            plt.savefig('nnOutput_'+featName+batchSizeStr+'.svg', bbox_inches='tight')
            plt.savefig('nnOutput_'+featName+batchSizeStr+'.eps', bbox_inches='tight')
            ax.set_yscale('log')
            yRange = (0.0001, ax.get_ylim()[1])
            ax.set_ylim(yRange)
            plt.savefig('nnOutput_'+featName+batchSizeStr+'_log.pdf', bbox_inches='tight')
            plt.savefig('nnOutput_'+featName+batchSizeStr+'_log.svg', bbox_inches='tight')
            plt.savefig('nnOutput_'+featName+batchSizeStr+'_log.eps', bbox_inches='tight')

            # Now in the original.
            #tempBinning = origScaleBinning[featI]
            tempBinning = [i*20 for i in range(0,41)]
            zoomBinning = [i*5 for i in range(0,31)]
            recoObjectsTestHist, featBinEdges      = np.histogram(recoObjectsTestOrigScale[:,featI], tempBinning)
            truthObjectsTestHist, featBinEdges     = np.histogram(truthObjectsTestOrigScale[:,featI], tempBinning)
            corrTruthObjectsTestHist, featBinEdges = np.histogram(corrTruthObjectsTestOrigScale[batchSize][:,featI], tempBinning)

            recoObjectsTrainHist, featBinEdges      = np.histogram(recoObjectsTrainOrigScale[:,featI], tempBinning)
            truthObjectsTrainHist, featBinEdges     = np.histogram(truthObjectsTrainOrigScale[:,featI], tempBinning)
            corrTruthObjectsTrainHist, featBinEdges = np.histogram(corrTruthObjectsTrainOrigScale[batchSize][:,featI], tempBinning)

            recoObjectsTestZoomHist, featBinEdgesZoom      = np.histogram(recoObjectsTestOrigScale[:, featI], zoomBinning)
            truthObjectsTestZoomHist, featBinEdgesZoom     = np.histogram(truthObjectsTestOrigScale[:, featI], zoomBinning)
            corrTruthObjectsTestZoomHist, featBinEdgesZoom = np.histogram(corrTruthObjectsTestOrigScale[batchSize][:, featI], zoomBinning)

            recoObjectsTrainZoomHist, featBinEdgesZoom      = np.histogram(recoObjectsTrainOrigScale[:, featI], zoomBinning)
            truthObjectsTrainZoomHist, featBinEdgesZoom     = np.histogram(truthObjectsTrainOrigScale[:, featI], zoomBinning)
            corrTruthObjectsTrainZoomHist, featBinEdgesZoom = np.histogram(corrTruthObjectsTrainOrigScale[batchSize][:, featI], zoomBinning)

            # Get rid of the first bin, which represents the inefficiency, if we are not looking
            # at the first feature. 
            if featI > 0:
                featBinEdges = featBinEdges[1:]
                recoObjectsTrainHist = recoObjectsTrainHist[1:]
                truthObjectsTrainHist = truthObjectsTrainHist[1:]
                corrTruthObjectsTrainHist = corrTruthObjectsTrainHist[1:]

                recoObjectsTestHist = recoObjectsTestHist[1:]
                truthObjectsTestHist = truthObjectsTestHist[1:]
                corrTruthObjectsTestHist = corrTruthObjectsTestHist[1:]

            compareDists([recoObjectsTestHist, corrTruthObjectsTestHist, truthObjectsTestHist],
                         ['Delphes', 'NN', 'Generator'],
                         featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test'+batchSizeStr,
                         featLabel, units=units[featI], ratioEndIndex=1,
                         ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")
            compareDists([recoObjectsTestHist, corrTruthObjectsTestHist, truthObjectsTestHist],
                         ['Delphes', 'NN', 'Generator'],
                         featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test'+batchSizeStr,
                         featLabel, log=False, units=units[featI],
                         ratioEndIndex=1, ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")

            compareDists([recoObjectsTrainHist, corrTruthObjectsTrainHist, truthObjectsTrainHist],
                         ['Delphes', 'NN', 'Generator'],
                         featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train'+batchSizeStr,
                         featLabel, units=units[featI], ratioEndIndex=1,
                         ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")
            compareDists([recoObjectsTrainHist, corrTruthObjectsTrainHist, truthObjectsTrainHist],
                         ['Delphes', 'NN', 'Generator'],
                         featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train'+batchSizeStr,
                         featLabel, log=False, units=units[featI],
                         ratioEndIndex=1, ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")

            if featI == 0:
                compareDists([recoObjectsTestZoomHist, corrTruthObjectsTestZoomHist, truthObjectsTestZoomHist],
                             ['Delphes', 'NN', 'Generator'],
                             featBinEdgesZoom, ['k', 'b', 'r'], 'jet',
                             featName+'_genVsReco_origScale_zoom_test'+batchSizeStr,
                             featLabel, units=units[featI], ratioEndIndex=1,
                             ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")
                compareDists([recoObjectsTestZoomHist, corrTruthObjectsTestZoomHist, truthObjectsTestZoomHist],
                             ['Delphes', 'NN', 'Generator'],
                             featBinEdgesZoom, ['k', 'b', 'r'], 'jet',
                             featName+'_genVsReco_origScale_zoom_test'+batchSizeStr,
                             featLabel, units=units[featI], ratioEndIndex=1,
                             ratioYLabel="Delphes/NN", log=False, markerfacecolors=['none' for i in range(3)], yLabel="Jets")
                
                compareDists([recoObjectsTrainZoomHist, corrTruthObjectsTrainZoomHist, truthObjectsTrainZoomHist],
                             ['Delphes', 'NN', 'Generator'],
                             featBinEdgesZoom, ['k', 'b', 'r'], 'jet',
                             featName+'_genVsReco_origScale_zoom_train'+batchSizeStr,
                             featLabel, units=units[featI],
                             ratioEndIndex=1, ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")
                
                compareDists([recoObjectsTrainZoomHist, corrTruthObjectsTrainZoomHist, truthObjectsTrainZoomHist],
                             ['Delphes', 'NN', 'Generator'],
                             featBinEdgesZoom, ['k', 'b', 'r'], 'jet',
                             featName+'_genVsReco_origScale_zoom_train'+batchSizeStr,
                             featLabel, log=False, units=units[featI],
                             ratioEndIndex=1, ratioYLabel="Delphes/NN", markerfacecolors=['none' for i in range(3)], yLabel="Jets")

            highEtaPtRes = (recoObjectsTrainOrigScale[highEtaIdxTrain,0]-truthObjectsTrainOrigScale[highEtaIdxTrain,0])/truthObjectsTrainOrigScale[highEtaIdxTrain,0]
            lowEtaPtRes = (recoObjectsTrainOrigScale[lowEtaIdxTrain,0]-truthObjectsTrainOrigScale[lowEtaIdxTrain,0])/truthObjectsTrainOrigScale[lowEtaIdxTrain,0]
            highEtaPtResNN = (corrTruthObjectsTrainOrigScale[batchSize][highEtaIdxTrain,0]-truthObjectsTrainOrigScale[highEtaIdxTrain,0])/truthObjectsTrainOrigScale[highEtaIdxTrain,0]
            lowEtaPtResNN = (corrTruthObjectsTrainOrigScale[batchSize][lowEtaIdxTrain,0]-truthObjectsTrainOrigScale[lowEtaIdxTrain,0])/truthObjectsTrainOrigScale[lowEtaIdxTrain,0]

            ptRes = (recoObjectsTrainOrigScale[:,0]-truthObjectsTrainOrigScale[:,0])/truthObjectsTrainOrigScale[:,0]
            ptResNN = (corrTruthObjectsTrainOrigScale[batchSize][:,0]-truthObjectsTrainOrigScale[:,0])/truthObjectsTrainOrigScale[:,0]

            if highEtaPtRes.shape[0] > 0 and highEtaPtResNN.shape[0] > 0:
                highEtaKS = scipy.stats.ks_2samp(highEtaPtRes, highEtaPtResNN)
                lowEtaKS = scipy.stats.ks_2samp(lowEtaPtRes, lowEtaPtResNN)
                highKsVals.append(highEtaKS)
                lowKsVals.append(lowEtaKS)
                print("high and low eta KS:", highEtaKS, lowEtaKS)

            tempBinning = [i*.03-0.4 for i in range(31)]
            highEtaPtResHist, edges = np.histogram(highEtaPtRes, np.array(tempBinning))
            lowEtaPtResHist, edges = np.histogram(lowEtaPtRes, np.array(tempBinning))
            highEtaPtResNNHist, edges = np.histogram(highEtaPtResNN, np.array(tempBinning))
            lowEtaPtResNNHist, edges = np.histogram(lowEtaPtResNN, np.array(tempBinning))

            tempLabel = "($p_{\mathrm{T}}^{\mathrm{reco}}$-$p_{\mathrm{T}}^{\mathrm{truth}}$)/$p_{\mathrm{T}}^{\mathrm{truth}}$"
            compareDists([lowEtaPtResHist, highEtaPtResHist, lowEtaPtResNNHist, highEtaPtResNNHist],
                         ['Delphes, $|\eta|<3.2$', 'Delphes, $|\eta|>3.2$','NN, $|\eta|<3.2$', 'NN, $|\eta|>3.2$'], 
                         np.array(tempBinning), ['k','r', 'k', 'r'], 'jet', 'pTResEta'+batchSizeStr, tempLabel,
                         markerstyles=['+', '+', 'o','o'], markerfacecolors=['none', 'none','none', 'none'],
                         plotRatio=False, log=False, yLabel='arbitrary units',
                         plotHist=[True, True, False, False], plotErr=[True, True, True, True], normalize=True)
          
            tempLabel = "$\sigma((p_{\mathrm{T}}^{\mathrm{reco}}-p_{\mathrm{T}}^{\mathrm{truth}})/p_{\mathrm{T}}^{\mathrm{truth}})$"
            tempBinning = [i*40 for i in range(16)]+[600+200*i for i in range(1,8)]
            compareRMSs([truthObjectsTrainOrigScale[:,0],
                         truthObjectsTrainOrigScale[:,0]],
                         [ptRes, ptResNN],
                         ['Delphes','NN'],
                        np.array(tempBinning), ['k','r'], ['o', 'o'],
                        'jet', 'pTRes'+batchSizeStr, '$p_{\mathrm{T}}^{\mathrm{truth}}$ [GeV]',
                        yLabel=tempLabel, yLog=False)
            compareRMSs([truthObjectsTrainOrigScale[highEtaIdxTrain,0],
                         truthObjectsTrainOrigScale[highEtaIdxTrain,0],
                         truthObjectsTrainOrigScale[lowEtaIdxTrain,0],
                         truthObjectsTrainOrigScale[lowEtaIdxTrain,0]],
                         [highEtaPtRes, highEtaPtResNN, lowEtaPtRes, lowEtaPtResNN],
                         ['Delphes $|\eta|>3.2','NN $|\eta|>3.2', 'Delphes $|\eta|<3.2','NN $|\eta|<3.2'],
                        np.array(tempBinning), ['k','r', 'k', 'r'], ['+', '+','o', 'o'],
                        'jet', 'pTResEtaComp'+batchSizeStr, '$p_{\mathrm{T}}^{\mathrm{truth}}$ [GeV]',
                        yLabel=tempLabel, yLog=False)
            
            tempLabel = "$<(p_{\mathrm{T}}^{\mathrm{reco}}-p_{\mathrm{T}}^{\mathrm{truth}})/p_{\mathrm{T}}^{\mathrm{truth}}>$"
            compareMeans(truthObjectsTrainOrigScale[:,0],
                         [ptRes, ptResNN],
                         ['Delphes','NN'],
                        np.array(tempBinning), ['k','r'], 
                        'jet', 'pTRes'+batchSizeStr, '$p_{\mathrm{T}}^{\mathrm{truth}}$ [GeV]',
                         yLabel=tempLabel, yLog=False)
            
    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(batchSizes, [ksVal[0] for ksVal in lowKsVals], 'ro')
    plt.plot(batchSizes, [ksVal[0] for ksVal in highKsVals], 'ko')
    ax.set_xlabel('batch size')
    ax.set_ylabel('KS value')
    ax.set_xscale('log')

    leg = plt.legend(['$|\eta|<3.2$', '$|\eta|>3.2$'], loc=0, borderaxespad=0.)
    plt.savefig('ks.pdf', bbox_inches='tight')
    plt.savefig('ks.svg', bbox_inches='tight')
    plt.savefig('ks.eps', bbox_inches='tight')
    

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='rfast008.h5')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toy', 'other'])
    parser.add_argument('--nFiles', type=int, help='How many files do we want to process', default=None)
    parser.add_argument('--nObjects', type=int, help='Number of objects', default=2000000)
    parser.add_argument('--modelName', help='Name of model.', default='model_multiClass')
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of objects to use for training', default=2000000)
    parser.add_argument('--nTestSamp', type=int, help='Number of objects to use for testing', default=400000)
    parser.add_argument('--epochs', type=int, nargs='+', help='Epochs, comma delimited. Must be the same length as batch sizes', default=[200])
    parser.add_argument('--batchSizes', type=int, nargs='+', help='Batch sizes, comma delimited. Must be the same length as epochs', default=[100])
    parser.add_argument('--nonResIndexStart', type=int, help='What is the index where non resolution variables start', default=4)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--nResCats', type=int, help='Number of resolution categories', default=100)
    parser.add_argument('--uniformityProp', type=int, help='Property which will be flattened during training. For example, we may want a flat pT distribution.', default=None)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='resMultiC')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')
    parser.add_argument('--trainingFeatures', nargs='+', help='Indices of the features you want to train on.', default=[0,1,2,3])
    parser.add_argument('--updateScalers', action='store_true', help='Update the scaler pickle file.')
    parser.add_argument('--resScalerPath', type=str, help='Path to resolution scaler', default='resScaler.pkl')
    parser.add_argument('--scalerPath', type=str, help='Path to input scaler', default='scaler.pkl')
    parser.add_argument('--saveTrainingSet', action='store_true', help='Save the training set in a separate h5 file.')

    args = parser.parse_args()
    trainingFeatures = args.trainingFeatures
    if trainingFeatures != [0,1]:
        trainingFeatures = [int(trainingFeature) for trainingFeature in trainingFeatures]
    
    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print("Producing new hyperparameter file.")
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
        except IOError as e:
            print("Couldn't load "+args.hyperParamFPath)
            print("Producing new hyperparameter file.")
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    
    if args.hyperParamKey not in allHyperParams:
        print("Couldn't find hyperparameter set with key", args.hyperParamKey)
        sys.exit()
        
    hyperParams = allHyperParams[args.hyperParamKey]
    hyperParams['outputDim'] = args.nResCats

    np.random.seed(args.randomSeed)
    resBins = [i*(1./(args.nResCats)) for i in range(args.nResCats)]
   
    # Load training and testing data
    data = prepMatchedDataRes(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed,
                              sourceType=args.sourceType, nFiles=args.nFiles,
                              resBins=resBins, nObjects=args.nObjects,uniformityProp=args.uniformityProp,
                              trainingFeatures=trainingFeatures, scalerPath=args.scalerPath, resScalerPath=args.resScalerPath,
                              updateScalers=args.updateScalers, saveTrainingSet=args.saveTrainingSet
                              );

    if 'train' in args.trainTest:
        train(data, args.modelName, args.modelFPath, hyperParams, args.sourceDSName, args.targetDSName,
              args.epochs, args.batchSizes, args.hyperParamKey, args.sourceType
        );
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, args.sourceDSName, args.targetDSName,
             args.hyperParamKey, args.sourceType, args.nResCats, args.epochs, args.batchSizes, trainingFeatures=trainingFeatures
        );
        
